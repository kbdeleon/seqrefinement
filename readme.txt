*********************
Usage Notes:
*********************

- seqrefinement.py requires python (tested with version 2.5.2 and 2.6.5) and biopython (tested with version 1.57)

- Note:  How python looks at the start of a sequence record from biopython varies from version 2.5.2 to 2.6.5.  This program was developed to deal with both methods. We cannot guarantee its usability with other versions of python.

- Help for this program can be read below or by typing 'python seqrefinement.py -h' into the terminal.

Help file:
Usage: Sequence refinement by length, Ns and primer errors, with optional trimming and quality score analysis.
By default this program will not trim or quality check.

Usage: python seqrefinement.py fasta_file qual_file minimum_length [options]
Example: python seqrefinement.py fasta.fas quality.qual 250 -t 250 -u On -q 27,30,32 -p 10,12,15 -o Fakefiles_

Options:
  -h, --help            show this help message and exit
  -t TRIM, --trim=TRIM  Input trim length if desired,  default is no trimming
                        or 'Off'
  -u QUALCHECK, --qualcheck=QUALCHECK
                        Change this to On if you'd like your files to be
                        quality checked
  -q QSCORE, --qscore=QSCORE
                        Input quality score(s) that you would like to see the
                        data for.  Separate multiple quality score with commas
                        (no spaces!) (example 25 or 25,27)
  -p PERCENT, --percent=PERCENT
                        Input in the percent of nucleotides allowed to be
                        below the quality score as a fraction (example 5 for
                        5%, 10 for 10%, etc).  Separate multiple quality score
                        with commas (no spaces!) (example 10 or 10,15)
  -f FPRIMER, --fprimer=FPRIMER
                        Forward primer sequence to search for, default is
                        Bacterial 16S FD1 for V1V2 region (530F for V6 region
                        is GTGCCAGCMGCNGCGG)
  -r RPRIMER, --rprimer=RPRIMER
                        Reverse primer sequence to search for, default is
                        Bacterial 16S 529R for the V3 region (1100R for V8
                        region is GGGTTNCGNTCGTTR
  -o OUT, --out=OUT     Start of each of the output files to be written

*********************
Default Parameters
*********************
-t	Off
-u 	Off
-q	25,27,30,32
-p	15,10
-f	AGAGTTTGATCCTGGCTCAG  (FD1)
-r	CGCGGCTGCTGGCAC  (529R)
-o	Input fasta file name minus the extension

*********************
Usage Examples:
*********************
- The following are two examples that are provided with this program.  All the files generated are in the file folders inside the Examples folder.  Screen shots are also provided in the Examples folder.

- Unzip the sequence files and put then in the Examples folder.  With the terminal called to the package folder, the commands below (don't copy the $) can be copied to the command-line and should run.

1)
$ python seqrefinement.py Examples/V1V2V3.fna Examples/V1V2V3.qual 246 -t 246 -u On -o Examples/V1V2V3_2715/V1V2V3_

When prompted, type 27 for quality score and 15 for percent allowed to be less than the quality score.



2) Does not use the default FD1 (V1V2) and 529R (V3) primers, so they must be entered.
$ python seqrefinement.py Examples/V4V6.fna Examples/V4V6.qual 253 -t 253 -u On -f GTGCCAGCMGCNGCGG -r GGGTTNCGNTCGTTR -o Examples/V4V6_3010/V4V6_

When prompted, type 30 for quality score and 10 for percent allowed to be less than the quality score.


*********************************************************************
Please send any comments or issues to kara.deleon22@gmail.com
*********************************************************************