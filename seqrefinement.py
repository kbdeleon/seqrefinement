#Note this program was written for Python 2.5.2 and 2.6.5 because of difference in how each of their .startswith works with sequence records.  
#We cannot guarantee the validity of using this program with any other versions.

# Creator:  Kara Bowen De Leon


from optparse import OptionParser
import sys
import Bio.SeqIO
import re
from Bio.Alphabet import IUPAC
from Bio.SeqRecord import SeqRecord
from Bio.Seq import Seq
from Bio.Data import IUPACData
from Bio import Alphabet

#Length and Trim (if turned on)
def seqlengthcutoff (fh,cutoff):
	fasta_reader=Bio.SeqIO.parse(fh,"fasta")
	longs=[]
	short=[]
	allcount=0
	for record in fasta_reader:
		allcount+=1
		if len(record.seq)>= cutoff:
			longs.append(record)
		else:
			short.append(record)			
	print "Initial Sequence Number: %s" % allcount
	return longs, short

def trimseqs (longs, trimlength):
	newrecord=""
	newfasta=[]
	short=0
	count=0
	for record in longs:
		newrecord=SeqRecord(seq=record.seq[0:trimlength], id=record.id, description="")
		newfasta.append(newrecord)
	return newfasta


#Quality-Checking

def fmt_trim_qual(fh, trim, lengthcutoff): # qual files usaully have returns in the middle of sequences, this removes them just title line and qual line for each seq, also trims if called for
	newfile=[]
	fmtfile=[]
	temp=[]
	count=1 #counts 1st seqname already
	firstline=True
	while True:
		line=fh.readline().strip("\n")
		if line=="":
			fmtfile.append(" ".join(temp)) #appends last line of qual data
			break
		elif firstline==True: #will append 1st seq name without needed previous sequences qual info
			firstline=False
			fmtfile.append(line) 
		elif line.startswith(">"): #will be title line
			count+=1
			fmtfile.append(" ".join(temp))  #1st appends compiled qual info from previous sequence and then appends new new seq name
			temp=[]
			fmtfile.append(line) # seq name
		else:
			temp.append(line)
	seqname=""
	qualout=[]
	short=0
	newqual=""
	for item in fmtfile:
		if item.startswith(">"):  
			seqname=item
		else:
			spl_item=item.split(" ")
			length=len(spl_item)
			if trim=="Off":
				if length<int(lengthcutoff):
					short+=1
					continue
				else:
					qualout.append(seqname)
					qualout.append(" ".join(spl_item))
			else:
				trimlength=int(trim)
				if length<trimlength:
					short+=1
					continue
				else:
					newqual=spl_item[0:trimlength]
					qualout.append(seqname)
					qualout.append(" ".join(newqual))
	if trim!="Off":
		print "\nQuality score trimming complete\n"
	return qualout


def percentqualinfo(fmtfile, qualcutoff):
	seqname=""
	seqqualinfo=[]
	for item in fmtfile:
		count=0
		if item.startswith(">"):
			seqname=item
		else:
			spl_item=item.split(" ")
			length=len(spl_item)
			for i in range(len(spl_item)):
				x=spl_item[i]
				y=int(x)
				if y < qualcutoff:
					count+=1.0
			percentlowqual= count/length
			temp=seqname[1:] + "\t" + "%.3f" % percentlowqual
			seqqualinfo.append(temp)
	return seqqualinfo


def percentcheck(seqqualinfo,percentallowed):
	lowqual=[]
	highqual=[]
	name=""
	percentlowqual=""
	for item in seqqualinfo:
		name,percentlowqual=item.split("\t")
		if float(percentlowqual) > percentallowed:
			lowqual.append(item)
		else:
			highqual.append(item)
#	lowqualout="\n".join(lowqual)
#	highqualout="\n".join(highqual)
	return lowqual, len(lowqual), highqual, len(highqual)
					
# Pull quality check results and generate qual and fasta files			
def pullqualnames(qualstats, qualityscore, percentcutoff):
	highqualnames=[]
	wantedtitleline="At quality score %s and %s percent cutoff\n" % (qualityscore, percentcutoff)
	found=False
	lines=[]
	for line in qualstats:
		#print line
		if found==False:	
			if line==wantedtitleline:
				found=True
			else:
				continue
		elif found==True and line=="\n\n": 
				return highqualnames
		else:
			titles=[]
			for i in line:
				if i==" ":
					break
				else:
					titles.append(i)
			highqualnames.append("".join(titles))
			continue

def pullpyroseq (seqnames,fasta):
	des=""
	reffasta=[]
	sequence=""
	count=5000
	for record in fasta:
		for name in seqnames:
			if name in record.id:
				seqnames.remove(name)
				reffasta.append(record)
				if len(reffasta)==count:
					print"%s sequences have been found. Still searching..." % count
					count+=5000
	return reffasta, seqnames  #allnames will be missing names at this point






#Ns and Primer Check
def findN (longs):
	withNs=[]
	withoutNs=[]
        for record in longs:
		if "N" in record.seq or "n" in record.seq:
			withNs.append(record)
		else:
			withoutNs.append(record)
	return withNs, withoutNs


#For Python 2.5.2 with primers that do not contain any ambiguous nucleotides
def primer252 (fh,Fprimer,Rprimer):  #Python 2.5.2 doesn't recognize to just look if the sequence startswith something, it looks at the start 
#which starts with "SEQ('" I added this onto the primer sequence.
	fasta_reader=Bio.SeqIO.parse(fh,"fasta")
	forward=[]
	reverse=[]
	error=[]
	for record in fasta_reader:
		rec=str(record.seq)
		REC=rec.upper()
		fdups=REC.count(Fprimer)  #these check for duplicate priming events
		fdups=int(fdups)
		rdups=REC.count(Rprimer)
		rdups=int(rdups)
		dups=fdups+rdups
		if dups >1:
			error.append(record)
		else: 
			if REC.startswith("SEQ('%s" % Fprimer):
				forward.append(record)
			elif REC.startswith("SEQ('%s" % Rprimer):
				reverse.append(record)
			else:
				error.append(record)
	return forward, reverse, error

#For Python 2.5.2 with primers containing ambiguous nucleotides
def ambprimer252 (fh, Fprimer, Rprimer):
	fasta_reader=Bio.SeqIO.parse(fh,"fasta")
	withamb1 = '' 
	withamb2= ''
	Fsearch= ''
	Rsearch= ''
	dups=[]
	forward= []
	reverse= []
	error= []
	duplicates=[]
	count=0
	for nt in Fprimer: 
		value = IUPACData.ambiguous_dna_values[nt] 
		if len(value) == 1: 
			withamb1 += value 
		else: 
			withamb1 += '[%s]' % value 
	for nt2 in Rprimer: 
		value2 = IUPACData.ambiguous_dna_values[nt2] 
		if len(value2) == 1: 
			withamb2 += value2 
		else: 
			withamb2 += '[%s]' % value2 
	withamb1=re.compile(withamb1)
	withamb2=re.compile(withamb2)
	for record in fasta_reader:
		Ffind=""
		Rfind=""
		rec=str(record.seq)
		REC=rec.upper()
		Fsearch = re.search(withamb1, REC[5:])
		Rsearch = re.search(withamb2, REC[5:])
		Fsearch2 = str(Fsearch)
		Rsearch2 = str(Rsearch)
		for match in re.finditer(withamb1,REC[6:]):  #these check for duplicate priming events
			Ffind=str(match)
		for match2 in re.finditer(withamb2,REC[6:]):
			Rfind=str(match2)
		if Ffind.startswith("<") or Rfind.startswith("<"):
			error.append(record)
		elif Fsearch2.startswith("<"):
			forward.append(record)
		elif Rsearch2.startswith("<"):
			reverse.append(record)
		else:
			error.append(record)
	return forward, reverse, error                        

#For Python 2.6.5 with primers that do not contain ambiguous nucleotides
def primer265 (fh,Fprimer,Rprimer):
	fasta_reader=Bio.SeqIO.parse(fh,"fasta")
	forward=[]
	reverse=[]
	error=[]
	for record in fasta_reader:
		rec=str(record.seq)
		REC=rec.upper()
		fdups=REC.count(Fprimer)
		fdups=int(fdups)
		rdups=REC.count(Rprimer)
		rdups=int(rdups)
		dups=fdups+rdups
		if dups >1:
			error.append(record)
		else: 
			if REC.startswith(Fprimer):
				forward.append(record)
			elif REC.startswith(Rprimer):
				reverse.append(record)
			else:
				error.append(record)
	return forward, reverse, error

#For Python 2.6.5 with primers that contain ambiguous nucleotides
def ambprimer265 (fh, Fprimer, Rprimer):
	fasta_reader=Bio.SeqIO.parse(fh,"fasta")
	withamb1 = '' 
	withamb2= ''
	Fsearch= ''
	Rsearch= ''
	dups=[]
	forward= []
	reverse= []
	error= []
	duplicates=[]
	count=0
	for nt in Fprimer: 
		value = IUPACData.ambiguous_dna_values[nt] 
		if len(value) == 1: 
			withamb1 += value 
		else: 
			withamb1 += '[%s]' % value 
	for nt2 in Rprimer: 
		value2 = IUPACData.ambiguous_dna_values[nt2] 
		if len(value2) == 1: 
			withamb2 += value2 
		else: 
			withamb2 += '[%s]' % value2 
	withamb1=re.compile(withamb1)
	withamb2=re.compile(withamb2)
	for record in fasta_reader:
		Ffind=""
		Rfind=""
		rec=str(record.seq)
		REC=rec.upper()
		Fsearch = re.search(withamb1, REC)
		Rsearch = re.search(withamb2, REC)
		Fsearch2 = str(Fsearch)
		Rsearch2 = str(Rsearch)
		for match in re.finditer(withamb1,REC[1:]):  #these check for duplicate priming events
			Ffind=str(match)
		for match2 in re.finditer(withamb2,REC[1:]):
			Rfind=str(match2)
		if Ffind.startswith("<") or Rfind.startswith("<"):
			error.append(record)
		elif Fsearch2.startswith("<"):
			forward.append(record)
		elif Rsearch2.startswith("<"):
			reverse.append(record)
		else:
			error.append(record)
	return forward, reverse, error


def pullqualseq (seqnames,qualfile):
	des=""
	newqual=[]
	count=0
	x=0
	qualcount=2000
	for line in qualfile:
		if line.startswith(">"):
			x=0
			for s in seqnames:
				if s in line:
					seqnames.remove(s)
					newqual.append(line)
					count+=1
					if count==qualcount:
						print "%s qual sequences pulled so far" % (count)
						qualcount+=2000
					x=1
		elif x==1:
			newqual.append(line)  #appends qual data lines
		else:
			continue
	return seqnames, newqual





def ParseArguments(argv):
	usage='Sequence refinement by length, Ns and primer errors, with optional trimming and quality score analysis.\nBy default this program will not trim or quality check.\n\nUsage: python %prog fasta_file qual_file minimum_length [options]\nExample: python seqrefinement.py fasta.fas quality.qual 250 -t 250 -u On -q 27,30,32 -p 10,12,15 -o Fakefiles_'				
	parser=OptionParser(usage)
	parser.add_option("-t", "--trim", default="Off", help="Input trim length if desired,  default is no trimming or 'Off'") 
	parser.add_option("-u", "--qualcheck", default="Off", help="Change this to On if you'd like your files to be quality checked")
	parser.add_option("-q", "--qscore", default="25,27,30,32", help="Input quality score(s) that you would like to see the data for.  Separate multiple quality score with commas (no spaces!) (example 25 or 25,27)")
	parser.add_option("-p", "--percent", default="15,10", help="Input in the percent of nucleotides allowed to be below the quality score as a fraction (example 5 for 5%, 10 for 10%, etc).  Separate multiple quality score with commas (no spaces!) (example 10 or 10,15)")
	parser.add_option("-f", "--fprimer", default="AGAGTTTGATCCTGGCTCAG", help="Forward primer sequence to search for, default is Bacterial 16S FD1 for V1V2 region (530F for V6 region is GTGCCAGCMGCNGCGG)") 		
	parser.add_option("-r", "--rprimer", default="CGCGGCTGCTGGCAC", help="Reverse primer sequence to search for, default is Bacterial 16S 529R for the V3 region (1100R for V8 region is GGGTTNCGNTCGTTR") 
	parser.add_option("-o", "--out", default=sys.argv[1][:-4], help="Start of each of the output files to be written")
	(options,args)=parser.parse_args()
	return (options,args)

options, args = ParseArguments(sys.argv)

if len(sys.argv)<2:
	parser.print_help()
	quit()

fh=open(sys.argv[1],"r")  #input fasta file
qualfile=open(sys.argv[2],"r") #input quality file
lengthcutoff=int(sys.argv[3])  #input minimum length requirement


#Length and Trim
Longset,Shortset=seqlengthcutoff(fh,lengthcutoff)
handle=open(options.out+"longseqs.fas","w")
Bio.SeqIO.write(Longset,handle,"fasta")
handle.close()
handle2=open(options.out+"shortseqs.fas","w")
Bio.SeqIO.write(Shortset,handle2,"fasta")
handle2.close()
fh.close()
print "\nLength check complete"
print "Sequences below length requirement: %s \nRemaining sequences: %s" % (len(Shortset), len(Longset))

qualtopull=""
percenttopull=""

if options.trim!="Off":
	Longset=trimseqs(Longset, int(options.trim))
	handle_t=open(options.out+"trimmedseqs.fas", "w")
	Bio.SeqIO.write(Longset, handle_t, "fasta")
	handle_t.close()
	print "\nSequence trimming complete"

SeqstocheckforNs=Longset

#Quality Checking
formattedqual=fmt_trim_qual(qualfile,options.trim, lengthcutoff)  #if trimming is set to off, this will just return a formatted version of the qual file with short ones removed;  if trim is turned on by putting in the trim length, this will also trim the qual seqs.
qualfile.close()

if options.qualcheck!="Off":
	qualcutoff=[options.qscore]	
	if "," in qualcutoff[0]:
		t=qualcutoff[0].split(",")
		qualcutoff=[]
		for i in t:
			qualcutoff.append(int(i))
	else:
		qualcutoff=[int(qualcutoff[0])]

	percentallowed=[options.percent]
	if "," in percentallowed[0]:
		p=percentallowed[0].split(",")
		percentallowed=[]
		for i in p:
			percentallowed.append(float(i)/100)
	else:
		percentallowed=[float(percentallowed[0])/100]

	lowqualfinal=[]
	highqualfinal=[]
	print "Q\t%\tLowQual\tHighQual"
	for i in range(len(qualcutoff)):
		seqqualinfo=percentqualinfo(formattedqual,int(qualcutoff[i]))
		for n in range(len(percentallowed)):
			lowqual,lowquallen, highqual, highquallen=percentcheck(seqqualinfo, percentallowed[n])
			print "%s \t %.0f \t %s \t %s \t" % (qualcutoff[i], percentallowed[n]*100, lowquallen, highquallen)
			lowtitle=["At quality score %s and %.0f percent cutoff\n" % (qualcutoff[i], percentallowed[n]*100)]
			lowqualfinal=lowqualfinal+ lowtitle + lowqual
			lowqualfinal.append("\n\n")
			hightitle=["At quality score %s and %.0f percent cutoff\n" % (qualcutoff[i], percentallowed[n]*100)]
			highqualfinal= highqualfinal + hightitle + highqual
			highqualfinal.append("\n\n")
	handle_l=open(options.out+"lowqualstats.txt","w")
	handle_l.write("".join(lowqualfinal))
	handle_l.close()
	handle_h=open(options.out+"highqualstats.txt","w")
	handle_h.write("".join(highqualfinal))
	handle_h.close()
	fh.close()

	qualtopull=raw_input("\nEnter a quality score from above you would like to use for the remainder of processing (e.g. 25):\t")
	assert int(qualtopull) in qualcutoff, "You selected a quality score that was not in the initial analysis"
	percenttopull=raw_input("Enter a percent allowed from above (as a whole number) you would like to use for the remainder of processing (e.g. 10): \t")
	assert float(percenttopull)/100 in percentallowed, "You selected a percent that was not in the initial analysis or improperly entered it in.  Please ensure that you write the percent as a whole number (ex 10 for 10%)."

#	print highqualfinal.split("\n")
	highqualnames=pullqualnames(highqualfinal,qualtopull,percenttopull)


	print "\nPulling sequences..."  #pull quality-checked seqs to check for Ns and primer errors
	highqualfasta,missingnames=pullpyroseq(highqualnames,Longset)
	print "Pulling sequences complete"
	SeqstocheckforNs=highqualfasta
	assert len(missingnames)==0, "For some reason, not all high quality sequences were found in the fasta file"



#Ns	
withNs, withoutNs= findN(SeqstocheckforNs)
handle3=open(options.out + str(qualtopull) + str(percenttopull) + "withNs.fas", "w")
Bio.SeqIO.write(withNs,handle3,"fasta")
handle3.close()
handle4=open(options.out + str(qualtopull) + str(percenttopull) + "withoutNs.fas", "w")
Bio.SeqIO.write(withoutNs,handle4,"fasta")
handle4.close()
print "\nCheck for Ns complete"
print "With Ns: %s \nWithout Ns: %s" % (len(withNs), len(withoutNs))
fh.close()


#primer test
fh=open(options.out + str(qualtopull) + str(percenttopull) + "withoutNs.fas", "r")
Fprimer=options.fprimer
Rprimer=options.rprimer
Fprimer2=str(Fprimer.upper())
Rprimer2=str(Rprimer.upper())
versiondata=sys.version
version=versiondata[0:3]
Ftest=[]
Rtest=[]

for nt in Fprimer2:
        if nt not in "AGTC":
               Ftest.append("amb")
        else:
                continue
for nt2 in Rprimer2:
        if nt2 not in "AGTC":
               Rtest.append("amb")
        else:
                continue
if version=="2.5":
	if "amb" in Ftest or "amb" in Rtest:
			F,R,E=ambprimer252(fh, Fprimer2, Rprimer2)
	else:
			F,R,E=primer252(fh, Fprimer2, Rprimer2)

else:

	if "amb" in Ftest or "amb" in Rtest:
			F,R,E=ambprimer265(fh, Fprimer2, Rprimer2)        
	else:
			F,R,E=primer265(fh, Fprimer2, Rprimer2)


handle=open(options.out + str(qualtopull) + str(percenttopull) + "forward.fas","w")
Bio.SeqIO.write(F,handle,"fasta")
handle.close()
handle2=open(options.out + str(qualtopull) + str(percenttopull) + "reverse.fas","w")
Bio.SeqIO.write(R,handle2,"fasta")
handle2.close()
handle3=open(options.out + str(qualtopull) + str(percenttopull) + "primererrors.fas","w")
Bio.SeqIO.write(E,handle3,"fasta")
handle3.close()
print "\nPrimer check complete"
print "Forward seq: %s\nReverse sequences: %s \nPrimer Error sequences: %s" % (len(F), len(R), len(E))

#Write refined Qual files
print "\nGenerating Qual Files"

Fnames=[]
Rnames=[]
for record in F:
	Fnames.append(record.id)

for record in R:
	Rnames.append(record.id)
print "Forward"
missingFnames, Fqual=pullqualseq(Fnames, formattedqual)
assert len(missingFnames)==0, "For some reason, not all of the Forward quality seqs were found in the qual file"
handle_fq=open(options.out + str(qualtopull) + str(percenttopull) + "forward.qual","w")
handle_fq.write("\n".join(Fqual))
handle_fq.close()
print "Reverse"
missingRnames, Rqual=pullqualseq(Rnames, formattedqual)
assert len(missingRnames)==0, "For some reason, not all of the Reverse quality seqs were found in the qual file"
handle_rq=open(options.out + str(qualtopull) + str(percenttopull) + "reverse.qual","w")
handle_rq.write("\n".join(Rqual))
handle_rq.close()

print "\nDone!\n"
